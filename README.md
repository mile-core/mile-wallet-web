# MILE Web wallet

### Environment
For testnet:
```
MILE_API_BASE=https://wallet.testnet.mile.global
```

For mainnet:
```
MILE_API_BASE=https://wallet.mile.global
```
or just leave it empty

### Quick start

`npm i`

`npm start`

### Pack files for production

`npm run build`

All needed files will be in `dist` folder
