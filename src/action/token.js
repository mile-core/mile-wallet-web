import api from '../Api';
import {changeTransactionLoading, getModule} from "./wallet";

const getWalletState = {
    id: 'get-wallet-state',
    params: {
        wallet_name: '',
        public_key: '',
    },
};

const setTokenNotFount = () => (dispatch) => {
    dispatch({
        type: 'SET_TOKEN_NOT_FOUND',
        tokenNotFount: true,
    });
};

const setNoInternetConnection = () => (dispatch) => {
    dispatch({
        type: 'SET_NO_INTERNET_CONNECTION',
        noInternet: true,
    });
};

const setWalletStateSuccess = () => (dispatch) => {
    dispatch({
        type: 'SET_WALLET_STATE_SUCCESS',
        walletStateSuccess: true,
    });
};

const setTransactionListStateSuccess = () => (dispatch) => {
    dispatch({
        type: 'SET_TRANSACTION_LIST_STATE_SUCCESS',
        transactionsListStateSuccess: true,
    });
};

const setSendTokenSuccess = (lastID) => (dispatch) => {
    dispatch({
        type: 'SET_SEND_TOKEN_SUCCESS',
        sendTokenSuccess: true,
        lastTransactionID: lastID,
    });
};

export const setDefaultTokenValues = ()=>(dispatch)=>{
    dispatch({
        type: 'SET_TOKEN_NOT_FOUND',
        tokenNotFount: false,
    });
    dispatch({
        type: 'SET_NO_INTERNET_CONNECTION',
        noInternet: false,
    });
    dispatch({
        type: 'SET_WALLET_STATE_SUCCESS',
        walletStateSuccess: false,
    });
    dispatch({
        type: 'SET_TRANSACTION_LIST_STATE_SUCCESS',
        transactionsListStateSuccess: false,
    });
    dispatch({
        type: 'SET_SEND_TOKEN_SUCCESS',
        sendTokenSuccess: false,
        lastTransactionID: null,
    });
    dispatch( setTransactions({
        transactions: [],
    }));
};

export const setWalletData = (data) => {
    return {
        type: 'SET_WALLET_DATA',
        payload: data,
    };
};

export const setTransactions = (data) => {
    return {
        type: 'SET_TRANSACTIONS',
        payload: data,
    };
};

export const walletState = (publicKey, privateKey) => (dispatch) => {
    try {
        api.getBalance(publicKey)
            .then((response) => {
                const balance = response.data.result.balance;
                let balanceMile = 0;
                let balanceXdr = 0;

                if (balance.length > 0) {
                    balance.forEach((balanceItem) => {
                        if (balanceItem.code === '1') {
                            balanceMile = parseFloat(balanceItem.amount).toFixed(5);
                        }

                        if (balanceItem.code === '0') {
                            balanceXdr = parseFloat(balanceItem.amount).toFixed(2);
                        }
                    });
                }

                dispatch(setWalletStateSuccess());
                dispatch(setWalletData({
                    publicKey: publicKey,
                    privateKey: privateKey,
                    balanceMile: balanceMile,
                    balanceXdr: balanceXdr,
                }));
            })
            .catch(() => {
                dispatch(setTokenNotFount());
            });
        }
        catch( err ){
            dispatch(setNoInternetConnection());
        }
};

export const sendToken = (params = {}) => (dispatch) => {
    dispatch(changeTransactionLoading(true));

    getModule().then( () => {
        const error = new Module.Error()
        const keyPairs = new Module.Pair.FromPrivateKey( params.privateKey, error )

        try{
            Promise.all([
                api.getLastBlockId( ),
                api.getBalance( keyPairs.public_key )
            ]).then( res => {
                var transaction = new Module.Transaction()

                transaction.Transfer(
                    keyPairs,
                    params.addressTo,
                    res[0].data.result['current-block-id'],
                    res[1].data.result['preferred-transaction-id'],
                    parseInt( params.currency, 10 ),
                    parseFloat( params.amountToSend ),
                    0.0,
                    params.memo,
                    error
                )

                api.sendTransaction( JSON.parse( transaction.body ) )
                    .then(() => {
                        dispatch(setSendTokenSuccess( res[1].data.result['preferred-transaction-id'] ) );
                    })
                    // .finally(() => {
                    //     dispatch(changeTransactionLoading(false));
                    // })
            })
            .finally(() => {
                dispatch(changeTransactionLoading(false));
            })
        }
        catch( err ){
            dispatch(setNoInternetConnection());
        }
    })
    .catch( () => {
        dispatch(changeTransactionLoading(false));
    })
}

export const issueXdr = ( params = {} ) => ( dispatch ) => {
    dispatch( changeTransactionLoading( true ) )

    getModule().then( () => {
        const error = new Module.Error()
        const keyPairs = new Module.Pair.FromPrivateKey( params.privateKey, error )

        try{
            Promise.all([
                api.getLastBlockId( ),
                api.getBalance( keyPairs.public_key )
            ]).then( res => {
                var transaction = new Module.Transaction()

                transaction.Emission(
                    keyPairs,
                    res[0].data.result['current-block-id'],
                    res[1].data.result['preferred-transaction-id'],
                    1,
                    0,
                    error
                )

                api.sendTransaction( JSON.parse( transaction.body ) )
                    .then( () => {
                        dispatch( setSendTokenSuccess( res[1].data.result['preferred-transaction-id'] ) )
                    })
                    // .finally(() => {
                    //     dispatch(changeTransactionLoading(false));
                    // })
            })
            .finally( () => {
                dispatch( changeTransactionLoading( false ) )
            })
        }
        catch( err ){
            dispatch( setNoInternetConnection( ) )
        }
    })
    .catch( () => {
        dispatch( changeTransactionLoading( false ) )
    })
}

export const getLastTransactions = ( publicKey, count = 1, silent = false ) => ( dispatch ) => {
    api.getLastTransactions( publicKey, count )
        .then( res => {
            if( res.data.result ){
                dispatch( setTransactionListStateSuccess() )
                dispatch( setTransactions({
                    transactions: res.data.result.transactions.filter( e => e.description['transaction-type'] == "TransferAssetsTransaction" ),
                }))
            }
        })
        .catch( () => {
            if( !silent ){
                dispatch( setTokenNotFount() )
            }
        })
}
