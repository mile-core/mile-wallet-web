//import api from '../Api';
//import {wallets} from "../apiJson/respons";

const initialState = {
    "id"     : "create-wallet",
    "result" : "0",
    "params" :
        {
            "private_key" : "",
            "public_key"  : ""
        }
};

const setCreateWalletSuccess = ()=>(dispatch)=>{
    dispatch({
        type: 'CREATE_WALLET_SUCCESS',
        createWalletSuccess: true
    });
};
export const setDefaultWalletsValues = ()=>(dispatch)=>{
    dispatch({
        type: 'CREATE_WALLET_SUCCESS',
        createWalletSuccess: false
    });
};

export const changeTransactionLoading = (loading)=>(dispatch)=>{
    dispatch({
        type: 'CHANGE_TRANSACTION_LOADING',
        payload: loading,
    });
};

export const createWallet = (params = {}) => (dispatch) => {
    getModule().then( () => {
        const error = new Module.Error()
        const keyPairs = new Module.Pair.Random( error )

        initialState.params.private_key = keyPairs.private_key;
        initialState.params.public_key = keyPairs.public_key;

        dispatch({
            type: 'SET_WALLET_SUCCESS',
            wallet: initialState,
        });

        dispatch(setCreateWalletSuccess());
    })
    .catch( () => {
        dispatch(setDefaultWalletsValues())
    })
};


export const getWalletKeys = (wallet) => (dispatch) => {
    dispatch({
        type: 'SET_WALLET_KEYS_SUCCESS',
        wallet,
    })
};

export const getWalletState = (wallet) => (dispatch) => {
    dispatch({
        type: 'SET_WALLET_STATE_SUCCESS',
        wallet,
    })
};

export const getTransationsListState = (wallet) => (dispatch) => {
    dispatch({
        type: 'SET_TRANSACTION_LIST_STATE_SUCCESS',
        wallet,
    })
};

export const getModule = () => {
    return new Promise( function( resolve, reject ) {
        if( Module && Module.calledRun ) {
            resolve( 'Success' )
            return
        }
        let count = 20
        const interval = setInterval( () => {
            if( !Module || count < 1 ) {
                reject( 'Error' )
                clearInterval( interval )
            }
            else if( Module.calledRun ) {
                resolve( 'Ready' )
                clearInterval( interval )
            }
            count -= 1
        }, 1000 )
      } )
}