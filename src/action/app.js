export const setAppLanguage = (language) => (dispatch) => {
    dispatch({
        type: 'SET_APP_LANGUAGE',
        language
    });
};