const initialState = {
    language: 'en'
};

export default (state = initialState, action) => {
    switch (action.type) {
        case "SET_APP_LANGUAGE":
            return {
                ...state,
                language : action.language
            };
        default:
            return state;
    }
};
