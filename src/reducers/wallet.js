export default (state = {
    publicKey: null,
    privateKey: null,
    balanceMile: 0,
    balanceXdr: 0,
    transactions: [],
    transactionLoading: false
}, action) => {
    switch (action.type) {
        case "SET_WALLET_SUCCESS":
            return {
                ...state,
                item : action.wallet
            };
        case "CREATE_WALLET_SUCCESS":
            return {
                ...state,
                createWalletSuccess : action.createWalletSuccess
            };

        case "SET_WALLET_DATA":
            return {
                ...state,
                publicKey: action.payload.publicKey,
                privateKey: action.payload.privateKey,
                balanceMile: action.payload.balanceMile,
                balanceXdr: action.payload.balanceXdr,
            };

        case "SET_TRANSACTIONS":
            return {
                ...state,
                transactions: action.payload.transactions,
            };

        case "CHANGE_TRANSACTION_LOADING":
            return {
                ...state,
                transactionLoading: action.payload,
            };

        default:
            return state;
    }
};
