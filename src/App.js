import React from 'react';
import {IntlProvider, addLocaleData} from 'react-intl';
import {Switch, Route, Redirect, withRouter} from 'react-router-dom';
import en from 'react-intl/locale-data/en';
import kn from 'react-intl/locale-data/kn';
import ja from 'react-intl/locale-data/ja';
import zh from 'react-intl/locale-data/zh';
import CreateWallet from  './containers/CreateWallet';
import localeData from  './i18n';
import SendToken from  './containers/SendToken';
import IssueXdr from  './containers/IssueXdr';
import WalletInfo from  './containers/WalletInfo';
import NavBar from './components/Header/Nav';
import Footer from './components/Footer/Footer';
import {connect} from "react-redux";

addLocaleData([...en, ...kn, ...ja, ...zh]);

class App extends React.Component {
    render() {
        const props = this.props;

        let language = this.props.language;
        let messages = localeData[language] || localeData.en;

        return (
            <IntlProvider key={language} locale={language} messages={messages}>
                <div>
                    <NavBar { ...props }/>
                    <hr className="nav-delimiter"/>
                    <Switch>
                        <Route exact path='/' component={CreateWallet}/>
                        <Route exact path='/send_coins' component={SendToken}/>
                        <Route exact path='/send_coins/:paymentDetails' component={SendToken}/>
                        {/*<Route exact path='/xdr_emission' component={IssueXdr}/>*/}
                        <Route exact path='/wallet_info' component={WalletInfo}/>
                        <Route exact path='/wallet_info/:publicKey' component={WalletInfo}/>
                        {/* <Route exact path='/v1/shared/wallet/key/public/:publicKey' component={WalletInfo}/> */}
                        <Redirect path='/v1/shared/payment/amount/:paymentDetails' to='/send_coins/:paymentDetails'/>
                        <Redirect path='/v1/shared/wallet/key/public/:publicKey' to="/wallet_info/:publicKey"/>
                        <Redirect to="/"/>
                    </Switch>
                    <hr  className="footer-delimiter"/>
                    <Footer/>
                </div>
            </IntlProvider>
        );
    }
}

export default withRouter(connect(
    (state) => ({
        language: state.app.language
    })
)(App));