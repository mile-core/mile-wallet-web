import React, {Component} from 'react';
import {connect} from 'react-redux';
import numeraljs from 'numeraljs';
import {
    FormGroup,
    Input,
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Alert,
    UncontrolledAlert,
    Label
} from 'reactstrap';
import {
    walletState,
    setDefaultTokenValues,
    sendToken,
    getLastTransactions,

} from "../action/token";
import {getModule} from '../action/wallet'
import {withRouter} from 'react-router-dom';
import _ from 'lodash';
import local from "../i18n/local";
import {FormattedMessage, injectIntl} from "react-intl";
import { sysParams } from '../config';

class SendToken extends Component {
    constructor(props){
        super(props);
        document.title = props.intl.formatMessage({
            id: local.sendCoins
        });
        this.state = {
            step:1,
            loading:false,
            privateKey: "",
            addressTo: "",
            amountToSend: "",
            transactionFee: "",
            memo: "",
            modal:false,
            modalMessage:"",
            modalConfirm: false,
            hasError: false,
            currency: "1",
            notifications : [],
            openInApp: false,
        };
        if( props.match.params.paymentDetails ) {
            const paymentDetails = props.match.params.paymentDetails.split(':');
            this.state.addressTo = paymentDetails[0];
            this.state.currency = paymentDetails[1] === 'XDR' ? '0' : '1';
            this.state.amountToSend = paymentDetails[2];
            this.state.openInApp = true;
        }
    };

    setInterval = null;
    flashAlert = (message, state = "danger")=>{
        let {notifications} = this.state;
        let THIS = this;
        if( _.findIndex( notifications, { 'text': message, 'isOpen': true } ) !== -1 ) {
            return;
        }
        notifications.push({text:message,isOpen:true,state});
        this.setState({
            notifications
        });
        if(notifications.length && this.setInterval === null){
            this.setInterval = setInterval(()=>{
                notifications.shift();
                THIS.setState({
                    notifications
                });
                if( notifications.length === 0 ){
                    clearInterval(this.setInterval);
                    this.setInterval = null;
                }
            },3000);
        }
    };

    onDismiss = (key)=> {
        let {notifications} = this.state;
        notifications[key].isOpen = false;
        this.setState({
            notifications
        });
    };

    /*Lifecycle --start*/
    componentWillReceiveProps(nextProps){
        const { tokenNotFount, noInternet, walletStateSuccess, sendTokenSuccess, lastTransactionID } = nextProps.token;

        if(tokenNotFount){
            this.flashAlert( nextProps.intl.formatMessage({
                id: local.alertKeyNotFound
            }), "danger");
            this.props.setDefaultTokenValues();
            this.setState({
                loading: false
            });
        }

        if(noInternet){
            this.flashAlert(nextProps.intl.formatMessage({
                id: local.alertNoInternet
            }),"danger");
            this.props.setDefaultTokenValues();
            this.setState({
                loading: false
            });
        }

        if(sendTokenSuccess){
            this.flashAlert(nextProps.intl.formatMessage({
                id: local.alertTokensSentSuccessfully
            }), "success");
            this.props.setDefaultTokenValues();
            this.setState({
                loading: false
            });
        }

        if( walletStateSuccess ){
            this.props.setDefaultTokenValues();
            this.setState({
                step: 2,
                loading: false
            });
        }

        if( lastTransactionID ){
            let tryCount = 6
            const intervalID = setInterval( () => {
                if( this.props.transactions.length > 0 &&
                    parseInt( this.props.transactions[0].description['transaction-id'] ) >= parseInt( lastTransactionID ) ) {
                    clearInterval( intervalID )
                    this.props.walletState( this.props.publicKey, this.props.privateKey )
                }
                else {
                    tryCount -= 1
                }
                if( tryCount <= 0 ){
                    this.props.walletState( this.props.publicKey, this.props.privateKey )
                    clearInterval( intervalID )
                }
                this.props.getLastTransaction( this.props.publicKey )
            }, 10000 )
        }
    }
    /*Lifecycle --start*/
    openWallet = (e) => {
        e.preventDefault();

        if (!this.privateKey.value) {
            this.flashAlert(this.props.intl.formatMessage({
                id: local.alertFillPrivateKey
            }));
            return;
        }

        if ( /[\u0081-\uFFFF]/.test( this.privateKey.value ) ) {
            this.flashAlert( this.props.intl.formatMessage({
                id: local.alertKeyNotFound
            }), 'danger' );
            return;
        }

        this.setState({
            hasError: false,
            modalMessage:'',
            loading: true
        });

        getModule().then( () => {
            const error = new Module.Error()
            const keyPairs = new Module.Pair.FromPrivateKey( this.privateKey.value, error )


            this.props.walletState(keyPairs.public_key, keyPairs.private_key);
        })
        .catch( () => {
            this.setState({
                hasError: true,
                modalMessage:'Error while loading',
                loading: false
            });
        })
    };

    confirmSend = () => {
        let params = {
            addressTo: this.addressTo.value,
            amountToSend: this.state.amountToSend,
            privateKey: this.props.privateKey,
            publicKey: this.props.publicKey,
            currency: this.state.currency,
            memo: this.memo.value.substring( 0, sysParams.memoLength ),
        };

        this.props.sendToken(params);
        this.addressTo.value = "";
        this.memo.value = "";

        this.setState({
            modal:false,
            modalMessage:"",
            modalConfirm: false,
            addressTo: "",
            amountToSend: "",
            transactionFee: "",
            memo: "",
            openInApp: false,
        })
    };

    send = (e) => {
        e.preventDefault();
        if(!this.addressTo.value){
            this.flashAlert(this.props.intl.formatMessage({
                id: local.alertFillAddressTo
            }));
            return;
        }

        if(!this.state.amountToSend){
            this.flashAlert(this.props.intl.formatMessage({
                id: local.alertFillAmountToSend
            }));
            return;
        }

        const currencyCode = this.state.currency === '0' ? 'XDR' : 'MILE';

        this.setState({
            modalConfirm: true,
            modal: true,
            modalMessage: (
                <FormattedMessage
                    id={local.confirmCoinSend}
                    values={{amount: this.state.amountToSend, currencyCode: currencyCode, addressTo: this.addressTo.value}}
                />
            ),
        });
    };

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    openInApp = (e) => {
        e.preventDefault();
        window.location.href = `mile-core://${window.location.host}/v1/shared/payment/amount/${this.props.match.params.paymentDetails}`;
    };


    handleAmountChange = (e) => {
        const amountToSend = parseFloat( e.target.value );


        if (
            !e.target.value
            || (this.state.currency === '1' && amountToSend+0.01 <= this.props.balanceMile)
            || (this.state.currency === '0' && amountToSend <= this.props.balanceXdr)
        ) {
            let trFee = ''
            if( amountToSend && this.state.currency === '1' ) {
                trFee = 0.01
            }
            this.setState({amountToSend: e.target.value, transactionFee: trFee});
        }
    };

    truncateAmount = ( amount, digs ) => {
        return Math.trunc( amount * (10**digs) ) / ( 10**digs )
    }


    handleCurrencyChange = (e) => {
        const curr = e.target.value
        let newAmount = this.state.amountToSend ? parseFloat( this.state.amountToSend ) : 0
        let trFee = ''
        if( curr === '0' ) {
            newAmount = this.truncateAmount( Math.min( newAmount, this.props.balanceXdr ), 2 )
        }
        else if( curr === '1' ) {
            newAmount = this.truncateAmount( Math.min( newAmount, Math.max( this.props.balanceMile - 0.01, 0 ) ), 5 )
            trFee = newAmount > 0 ? 0.01 : 0
        }
        this.setState({
            currency: e.target.value,
            amountToSend: newAmount,
            transactionFee: trFee
        });
    };


    render(){
        const { balanceMile, balanceXdr, publicKey, transactionLoading } = this.props;

        return(
            <section>
                <div className="py-5 text-white text-center send-token">
                    <div className="container">
                        {this.state.step === 1 &&
                            <div className="row card">
                                <div className="col-md-6 offset-md-3 col-xs-12">
                                    {this.state.openInApp  &&
                                    <button
                                        onClick={this.openInApp}
                                        className="btn btn-primary open-app-btn"
                                        disabled={this.state.loading}
                                    >
                                        <FormattedMessage id={local.openInApp} />
                                    </button>
                                    }
                                </div>
                                <div className="col-md-6 offset-md-3 col-xs-12">
                                    <h1 className="mb-4 text-center">
                                        <FormattedMessage id={local.sendCoins} />
                                        <br/>
                                    </h1>
                                </div>

                                <div className="col-md-6 offset-md-3 col-xs-12">
                                    <form onSubmit={this.openWallet}>
                                        <FormGroup>
                                            <Input
                                                type="textarea"
                                                name="private_key"
                                                defaultValue={this.state.privateKey}
                                                innerRef={key => (this.privateKey = key )}
                                                id="private_key"
                                                placeholder={this.props.intl.formatMessage({
                                                    id: local.privateKey
                                                })}
                                                autoComplete="off"
                                                disabled={this.state.loading}
                                            />
                                        </FormGroup>

                                        {this.state.hasError &&
                                            <Alert color="danger">{this.state.modalMessage}</Alert>
                                        }
                                        {this.state.loading === false &&
                                        <button
                                            type="submit"
                                            className="btn btn-lg btn-secondary open-wallet-btn"
                                        >
                                            <FormattedMessage id={local.openWallet} />
                                        </button>
                                        }
                                        {this.state.loading && <div className="lds-facebook"><div></div><div></div><div></div></div>}

                                        <br/>
                                        <br/>
                                        <p className="lead mb-4 text-dark">
                                            <FormattedMessage id={local.mileToXdrRate} />: 1.21
                                        </p>
                                        <p className="desc">
                                            <FormattedMessage id={local.warningDomainCheck} />
                                            <br/>
                                        </p>
                                    </form>
                                </div>
                            </div>
                        }

                        { this.state.step === 2 &&
                        <div className="row card">
                            <div className="col-md-6 offset-md-3 col-xs-12">
                                <h1 className="mb-4">
                                    <FormattedMessage id={local.sendCoins} />
                                    <br/></h1>
                            </div>
                            <div className="col-md-6 offset-md-3 col-xs-12">
                                <form>
                                    <p className="text-left">
                                        <FormattedMessage id={local.yourBalance} />:
                                        <br/>
                                        MILE: {numeraljs(balanceMile).format('0,0.00000')}
                                        <br/>
                                        XDR: {numeraljs(balanceXdr).format('0,0.00')}
                                    </p>

                                    <p className="text-left">
                                        <FormattedMessage id={local.yourAddress} />: { publicKey } </p>
                                    <FormGroup>
                                        <Input
                                            type="textarea"
                                            name="address_to"
                                            id="address_to"
                                            placeholder={this.props.intl.formatMessage({
                                                id: local.addressTo
                                            })}
                                            defaultValue={this.state.addressTo}
                                            innerRef={key => (this.addressTo = key )}
                                            autoComplete="off"
                                        />
                                    </FormGroup>

                                    <FormGroup>
                                        <Input
                                            onChange={this.handleCurrencyChange}
                                            value={this.state.currency}
                                            bsSize="lg"
                                            type="select"
                                            name="select"
                                            id="currency"
                                        >
                                            <option value="0">XDR</option>
                                            <option value="1">MILE</option>
                                        </Input>
                                    </FormGroup>

                                    <FormGroup>
                                        <Input
                                            type="number"
                                            min="0"
                                            name="amount"
                                            id="amount"
                                            placeholder={this.props.intl.formatMessage({
                                                id: local.amountToSend
                                            })}
                                            value={this.state.amountToSend}
                                            onChange={this.handleAmountChange}
                                            autoComplete="off"
                                        />
                                    </FormGroup>

                                    {this.state.transactionFee !== '' && this.state.transactionFee !== 0 &&
                                        <p className="text-left">
                                            <FormattedMessage id={local.transactionFee} />: {numeraljs(this.state.transactionFee).format('0,0.00000')}
                                            <br/>
                                        </p>
                                    }

                                    <FormGroup>
                                        <Input
                                            type="textarea"
                                            name="memo"
                                            id="memo"
                                            placeholder={this.props.intl.formatMessage({
                                                id: local.memo
                                            })}
                                            defaultValue={this.state.memo}
                                            innerRef={key => (this.memo = key )}
                                            autoComplete="off"
                                            maxLength={sysParams.memoLength}
                                        />
                                    </FormGroup>

                                    <Button onClick={(e)=>{this.send(e)}} color="secondary" className="send-wallet-btn">
                                        <FormattedMessage id={local.send} />
                                    </Button>

                                    <br/>
                                    <br/>
                                    <p className="lead mb-4 text-dark">
                                        <FormattedMessage id={local.mileToXdrRate} />: 1.21
                                    </p>
                                </form>
                            </div>
                        </div>}
                    </div>
                </div>

                <div className="error-popup">{
                    this.state.notifications.map((value,key) => {
                        return (<UncontrolledAlert color={value.state} key = {"UncontrolledAlert" + key} isOpen={value.isOpen} toggle={()=>{this.onDismiss(key)} } >
                            {value.text}
                        </UncontrolledAlert>)
                    })
                }</div>

                <Modal
                    isOpen={this.state.modal || transactionLoading}
                    toggle={()=>{this.toggle()}}
                    className={this.props.className}
                >
                    <ModalHeader toggle={()=>{this.toggle()}} className={""}>
                        <FormattedMessage id={local.sendCoins} />
                    </ModalHeader>

                    <ModalBody className={""}>
                        {transactionLoading ? (
                            <FormattedMessage id={local.sendingInProgress} />
                        ) : this.state.modalMessage}
                    </ModalBody>

                    {this.state.modalConfirm  &&
                        <ModalFooter className={""}>
                            <Button
                                color="secondary"
                                disabled={transactionLoading}
                                onClick={()=>{this.confirmSend()}}
                            >
                                <FormattedMessage id={local.yes} />
                            </Button>

                            <Button color="red" className="btn-cancel-modal" onClick={()=>{this.toggle()}}>
                                <FormattedMessage id={local.cancel} />
                            </Button>
                        </ModalFooter>
                    }
                </Modal>
            </section>
        );
    }
}

export default connect(
    (state) => ({
        token: state.token,
        balanceMile: state.wallet.balanceMile,
        balanceXdr: state.wallet.balanceXdr,
        publicKey: state.wallet.publicKey,
        privateKey: state.wallet.privateKey,
        transactionLoading: state.wallet.transactionLoading,
        transactions: state.wallet.transactions,
    }),
    (dispatch) => ({
        walletState: (publicKey, privateKey) => dispatch(walletState(publicKey, privateKey)),
        sendToken: (params) => dispatch(sendToken(params)),
        setDefaultTokenValues: () => dispatch(setDefaultTokenValues()),
        getLastTransaction: ( publicKey ) => dispatch( getLastTransactions( publicKey, 1, true ) ),
    }),
)(withRouter(injectIntl(SendToken)));
