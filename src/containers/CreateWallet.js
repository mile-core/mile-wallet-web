import React , { Component}from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {
    Button,
    FormGroup,
    Input,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Popover,
    PopoverBody,
    Label
} from 'reactstrap';

import {
    createWallet,
    setDefaultWalletsValues
} from "../action/wallet";
import {CopyToClipboard} from 'react-copy-to-clipboard';
import local from "../i18n/local";
import {FormattedMessage, injectIntl} from "react-intl";

class CreateWallet extends Component {
    constructor(props) {
        super(props);
        document.title = props.intl.formatMessage({
            id: local.createWallet
        });
        this.state = {
            walletName: '',
            modal:false,
            publicPopoverOpen: false,
            privatePopoverOpen: false,
            buttonPressed: false
        };
    }

    /*Lifecycle --start*/
    componentWillReceiveProps(nextProps){
        const { createWalletSuccess } = nextProps.wallet;
        this.setState({
            buttonPressed: false
        })
        if(createWalletSuccess){
            this.toggle();
            this.props.setDefaultWalletsValues();
        }
    }
    /*Lifecycle --end*/
    toggle = () => {
        this.setState({
            modal: !this.state.modal,
            popoverOpen: false
        })
    };
    publicPopoverToggle = ()=>{
        this.setState({
            publicPopoverOpen: !this.state.publicPopoverOpen
        });
    };
    privatePopoverToggle = ()=>{
        this.setState({
            privatePopoverOpen: !this.state.privatePopoverOpen
        });
    };

    create = (e) => {
        this.setState({
            buttonPressed: true
        });
        e.preventDefault();
        this.props.createWallet();
    };

    render(){
        const wallet = this.props.wallet.item;
        return(
            <div className="py-5 text-white text-center create-wallet">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6 offset-md-3 col-xs-12">
                            <h1 className="text-dark">
                                <FormattedMessage id={local.mileWallet} />
                            </h1>
                            <p className="lead mb-4 text-dark">
                                <FormattedMessage id={local.mileWalletIntro} />
                                <br/>
                            </p>
                            <form className="form-inline justify-content-center">
                                {this.state.buttonPressed === false &&
                                    <button type="submit" className="btn btn-lg btn-secondary create-wallet-btn" onClick={(e)=>{this.create(e)}}>
                                        <FormattedMessage id={local.createWallet} />
                                        <br/>
                                    </button>
                                }
                                {this.state.buttonPressed && <div className="lds-facebook"><div></div><div></div><div></div></div>}
                            </form>
                            <br/>
                            <p className="lead mb-4 text-dark">
                                <FormattedMessage id={local.mileToXdrRate} />: 1.21
                            </p>
                        </div>
                    </div>
                    <Modal isOpen={this.state.modal} toggle={()=>{this.toggle()}} className={this.props.className}>
                        <ModalHeader toggle={()=>{this.toggle()}} className={""}>
                            <FormattedMessage id={local.createWallet} />
                        </ModalHeader>
                        <ModalBody className={""} >
                            <FormGroup>
                                <Label htmlFor="public-key">
                                    <FormattedMessage id={local.publicKey} />
                                </Label>

                                <InputGroup>
                                    <Input
                                        type="textarea"
                                        name="public-key"
                                        defaultValue={ wallet && wallet.params.public_key }
                                        disabled
                                        autoComplete="off"
                                    />
                                    <InputGroupAddon addonType="append" className="copy_token">
                                        <InputGroupText onClick={()=>{this.privatePopoverToggle();}}>
                                            <CopyToClipboard text={ wallet && wallet.params.public_key}>
                                                <i className="fa fa-files-o fa-2x" aria-hidden="true" id="Popover1" />
                                            </CopyToClipboard></InputGroupText>
                                    </InputGroupAddon>
                                </InputGroup>
                            </FormGroup>

                            <Popover placement={"bottom"} isOpen={this.state.privatePopoverOpen} target="Popover1" delay={500}
                                     toggle={() => {
                                         this.privatePopoverToggle()
                                     }}>
                                <PopoverBody>
                                    <FormattedMessage id={local.keyCopiedSuccessfully} />
                                </PopoverBody>
                            </Popover>

                            <FormGroup>
                                <Label htmlFor="private-key">
                                    <FormattedMessage id={local.privateKey} />
                                </Label>

                                <InputGroup>
                                    <Input
                                        type="textarea"
                                        name="private-key"
                                        defaultValue={ wallet && wallet.params.private_key }
                                        disabled
                                        autoComplete="off"
                                    />
                                    <InputGroupAddon addonType="append" className="copy_token">
                                        <InputGroupText onClick={()=>{
                                            this.publicPopoverToggle();
                                        }}>
                                            <CopyToClipboard text={ wallet && wallet.params.private_key}>
                                                <i className="fa fa-files-o fa-2x" aria-hidden="true" id="Popover2" />
                                            </CopyToClipboard></InputGroupText>
                                    </InputGroupAddon>
                                </InputGroup>
                            </FormGroup>

                            <Popover placement={"bottom"} isOpen={this.state.publicPopoverOpen} target="Popover2" delay={500}
                                     toggle={() => {
                                         this.publicPopoverToggle()
                                     }}>
                                <PopoverBody>
                                    <FormattedMessage id={local.keyCopiedSuccessfully} />
                                </PopoverBody>
                            </Popover>

                            <p className="desc text-dark">
                                <FormattedMessage id={local.privateKeyCaution} />
                                <br/>
                            </p>
                        </ModalBody>
                        <ModalFooter className={""}>
                            <Button color="secondary" href="/send_coins">
                                <FormattedMessage id={local.sendCoins} />
                            </Button>
                        </ModalFooter>
                    </Modal>
                </div>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        wallet: state.wallet,
    }),
    (dispatch) => ({
        createWallet: () => dispatch(createWallet()),
        setDefaultWalletsValues: () => dispatch(setDefaultWalletsValues()),
    }),
)(withRouter(injectIntl(CreateWallet)));