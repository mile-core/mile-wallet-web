import React, {Component} from 'react';
import {connect} from 'react-redux';
import numeraljs from 'numeraljs';
import {
    FormGroup,
    Input,
    Alert,
    UncontrolledAlert
} from 'reactstrap';
import {
    walletState,
    setDefaultTokenValues,
    getLastTransactions
} from "../action/token";
import {Table, TableCell} from 'semantic-ui-react';
import {withRouter} from 'react-router-dom';
import _ from 'lodash';
import {FormattedMessage, injectIntl} from "react-intl";
import local from "../i18n/local";

class WalletInfo extends Component {
    constructor(props){
        super(props);
        document.title = props.intl.formatMessage({
            id: local.walletInfo
        });
        this.state = {
            step:1,
            loading:false,
            modal:false,
            modalMessage:"",
            modalConfirm: false,
            hasError: false,
            notifications : [],
            publicKey: this.props.match.params.publicKey
        };
    };

    setInterval = null;
    flashAlert = (message, state = "danger")=>{
        let {notifications} = this.state;
        let THIS = this;
        if( _.findIndex( notifications, { 'text': message, 'isOpen': true } ) !== -1 ) {
            return;
        }
        notifications.push({text:message,isOpen:true,state});
        this.setState({
            notifications
        });
        if(notifications.length && this.setInterval === null){
            this.setInterval = setInterval(()=>{
                notifications.shift();
                THIS.setState({
                    notifications
                });
                if( notifications.length === 0 ){
                    clearInterval(this.setInterval);
                    this.setInterval = null;
                }
            },3000);
        }
    };

    onDismiss = (key)=> {
        let {notifications} = this.state;
        notifications[key].isOpen = false;
        this.setState({
            notifications
        });
    };

    /*Lifecycle --start*/
    componentWillReceiveProps(nextProps){
        const { tokenNotFount , walletStateSuccess, noInternet } = nextProps.token;
        if(tokenNotFount){
            this.flashAlert(nextProps.intl.formatMessage({
                id: local.alertKeyNotFound
            }),"danger");
            this.props.setDefaultTokenValues();
            this.setState({
                loading: false
            });
        }
        if(noInternet){
            this.flashAlert(nextProps.intl.formatMessage({
                id: local.alertNoInternet
            }),"danger");
            this.props.setDefaultTokenValues();
            this.setState({
                loading: false
            });
        }
        if( walletStateSuccess ){
            // this.props.setDefaultTokenValues();
            this.setState({
                step: 2,
                loading: false
            });
        }
    }
    /*Lifecycle --start*/
    openWallet = (e) => {
        e.preventDefault();

        if (!this.publicKey.value) {
            this.flashAlert(this.props.intl.formatMessage({
                id: local.alertFillPublicKey
            }));
            return;
        }

        this.setState({
            hasError: false,
            modalMessage:'',
            loading: true
        });

        this.props.walletState(this.publicKey.value);
        this.props.transactionList(this.publicKey.value);
    };

    newWallet = (e) => {
        e.preventDefault();
        this.props.setDefaultTokenValues();
        this.setState({
            step: 1,
            publicKey: ''
        });
    };

    openInApp = (e) => {
        e.preventDefault();
        window.location.href = `mile-core://${window.location.host}/v1/shared/wallet/key/public/${this.props.match.params.publicKey}`;
    };

    render(){
        const { balanceMile, balanceXdr, publicKey, transactions } = this.props;

        return(
            <section>
                <div className="py-5 text-white text-center send-token">
                    <div className="container">
                        { this.state.step === 1 &&
                            <div className="row card">
                                <div className="col-md-6 offset-md-3 col-xs-12">
                                    {this.state.publicKey  &&
                                    <button
                                        onClick={this.openInApp}
                                        className="btn btn-primary open-app-btn"
                                        disabled={this.state.loading}
                                    >
                                        <FormattedMessage id={local.openInApp} />
                                    </button>
                                    }
                                </div>
                                <div className="col-md-6 offset-md-3 col-xs-12">
                                    <h1 className="mb-4 text-center">
                                        <FormattedMessage id={local.walletInfo} />
                                        <br/>
                                    </h1>
                                </div>

                                <div className="col-md-6 offset-md-3 col-xs-12">
                                    <form onSubmit={this.openWallet}>
                                        <FormGroup>
                                            <Input
                                                type="textarea"
                                                name="public_key"
                                                defaultValue={this.state.publicKey}
                                                innerRef={key => (this.publicKey = key )}
                                                id="public_key"
                                                placeholder={this.props.intl.formatMessage({
                                                    id: local.publicKey
                                                })}
                                                autoComplete="off"
                                                disabled={this.state.loading}
                                            />
                                        </FormGroup>

                                        {this.state.hasError &&
                                            <Alert color="danger">{this.state.modalMessage}</Alert>
                                        }
                                        {this.state.loading === false &&
                                        <button
                                            type="submit"
                                            className="btn btn-lg btn-secondary open-wallet-btn"
                                            disabled={this.state.loading}
                                        >
                                            <FormattedMessage id={local.walletInfo} />
                                        </button>
                                        }
                                        {this.state.loading && <div className="lds-facebook"><div></div><div></div><div></div></div>}

                                        <br/>
                                        <br/>
                                        <p className="lead mb-4 text-dark">
                                            <FormattedMessage id={local.mileToXdrRate} />: 1.21
                                        </p>
                                        <p className="desc">
                                            <FormattedMessage id={local.warningDomainCheck} />
                                            <br/>
                                        </p>
                                    </form>
                                </div>
                            </div>
                        }

                        { this.state.step === 2 &&
                        <div className="row card">
                            <div className="col-md-8 offset-md-2 col-xs-12">
                                <h1 className="mb-4">
                                    <FormattedMessage id={local.walletInfo} />
                                    <br/></h1>
                            </div>
                            <div className="col-md-8 offset-md-2 col-xs-12">
                                <p className="text-left">
                                    <FormattedMessage id={local.address} />: { publicKey }
                                </p>
                                <p className="text-left">
                                    <FormattedMessage id={local.balance} />:
                                    <br/>
                                    MILE: {numeraljs(balanceMile).format('0,0.00000')}
                                    <br/>
                                    XDR: {numeraljs(balanceXdr).format('0,0.00')}
                                </p>
                            </div>
                            { transactions.length > 0 &&
                            <div className="col-md-8 offset-md-2 col-xs-12">
                                <h2 className="mb-4">
                                    <FormattedMessage id={local.lastTransactions} />
                                    <br/></h2>
                                <Table basic="very" compact>
                                    <Table.Header>
                                        <Table.Row>
                                            <Table.HeaderCell width={3}>
                                                <FormattedMessage id={local.amount} />
                                            </Table.HeaderCell>
                                            <Table.HeaderCell width={10}>
                                                <FormattedMessage id={local.address} />
                                            </Table.HeaderCell>
                                        </Table.Row>
                                    </Table.Header>
                                    <Table.Body>
                                    { transactions.map( item =>
                                        <Table.Row textAlign="left" key={item.description['transaction-id']} className={"trans-"+item.type} id={"transID-"+item.description['transaction-id']}>
                                            <Table.Cell>
                                                {parseFloat(item.description.asset[0].amount).toFixed(item.description.asset[0].code==='1'?5:2)} {item.description.asset[0].code==='1'?'MILE':'XDR'}
                                            </Table.Cell>
                                            <TableCell>
                                                {publicKey && item.description.from && publicKey.trim()===item.description.from.trim() ? '\u2192 '+item.description.to : '\u2190 '+item.description.from}
                                            </TableCell>
                                        </Table.Row>
                                    )}
                                    </Table.Body>
                                </Table>
                            </div>
                            }
                            <div className="col-md-8 offset-md-2 col-xs-12">
                                <br/>
                                <form onSubmit={this.newWallet}>
                                    <button
                                        type="submit"
                                        className="btn btn-secondary open-wallet-btn"
                                    >
                                        <FormattedMessage id={local.anotherWallet} />
                                    </button>
                                </form>
                                <br/>
                                <p className="lead mb-4 text-dark">
                                    <FormattedMessage id={local.mileToXdrRate} />: 1.21
                                </p>
                            </div>
                        </div>}
                    </div>
                </div>

                <div className="error-popup">{
                    this.state.notifications.map((value,key) => {
                        return (<UncontrolledAlert color={value.state} key = {"UncontrolledAlert" + key} isOpen={value.isOpen} toggle={()=>{this.onDismiss(key)} } >
                            {value.text}
                        </UncontrolledAlert>)
                    })
                }</div>

            </section>
        );
    }
}

export default connect(
    (state) => ({
        token: state.token,
        balanceMile: state.wallet.balanceMile,
        balanceXdr: state.wallet.balanceXdr,
        publicKey: state.wallet.publicKey,
        transactions: state.wallet.transactions,
    }),
    (dispatch) => ({
        walletState: (publicKey) => dispatch(walletState(publicKey, '')),
        transactionList: (publicKey) => dispatch(getLastTransactions(publicKey, 10, true)),
        setDefaultTokenValues: () => dispatch(setDefaultTokenValues()),
    }),
)(withRouter(injectIntl(WalletInfo)));
