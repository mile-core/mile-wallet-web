import React, {Component} from 'react';
import {Button, FormFeedback, Input} from "reactstrap";
import {FormattedMessage, injectIntl} from "react-intl";
import local from "../../i18n/local";

class InputAmount extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: props.value,
            error: null
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.value !== this.props.value){
            this.setValue(nextProps.value)
        }
    }

    onChangeHandle(e){
        const value = parseFloat(e.target.value);
        this.setValue(value);
    }

    setValue(value){
        let error = null;

        if(!value){
            value = 0;
        }

        if(this.props.max !== undefined){
            if(this.getTotal(value) > this.props.max){
                error = new Error(this.props.intl.formatMessage({
                    id: local.maximumAmountReachedError
                }));
            }
        }

        this.setState({
            value,
            error
        })
    }

    getMaxValue(){
        if(!this.props.max){
            return 0;
        }

        return this.props.max - this.getFee(this.props.max)
    }

    getTotal(value = this.state.value){
        return value + this.getFee(value);
    }

    getFee(value = this.state.value){
        return value * (0.2 / 100);
    }

    setMaxValue(){
        this.setValue(this.getMaxValue());
    }

    renderFeedback(){
        const fee = this.getFee();

        if(this.state.error){
            return (
                <FormFeedback
                    invalid="true"
                    tooltip="true">

                    {this.state.error.message}
                </FormFeedback>
            )
        }

        if(fee){
            return (
                <FormFeedback
                    valid="true"
                    tooltip="true">

                    <FormattedMessage
                        id={local.feeAmountLabel}
                        values={
                            {
                                fee: fee.toFixed(3),
                                total: this.getTotal().toFixed(3)
                            }
                        }
                    />
                </FormFeedback>
            )
        }

        return null;
    }

    render() {
        const invalid = !!this.state.error;
        const feedback = this.renderFeedback();
        const maxValue = this.getMaxValue();

        return (
            <React.Fragment>
                <Input
                    valid={!invalid && feedback}
                    invalid={invalid}
                    min={0}
                    type="number"
                    autoComplete="off"
                    placeholder={this.props.intl.formatMessage({
                        id: local.amount
                    })}
                    {...this.props}
                    value={this.state.value}
                    onChange={this.onChangeHandle.bind(this)}

                    onBlur={() => {
                        this.props.onBlurHandle && this.props.onBlurHandle();
                    }}
                />

                {feedback}


                {maxValue && maxValue !== this.state.value ? (
                    <Button color="link" onClick={(e) => {
                        e.preventDefault();

                        this.setMaxValue();
                    }}>
                        <FormattedMessage
                            id={local.setMaximumValueLabel}
                            values={{maximumValue: this.getMaxValue()}}
                        />
                    </Button>
                ) : (
                    null
                )}

            </React.Fragment>
        );
    }
}

export default injectIntl(InputAmount);
