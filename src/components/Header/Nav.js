import React ,{ Component } from 'react';
import {Link} from 'react-router-dom';
import {FormattedMessage} from 'react-intl';
import local from '../../i18n/local';
import icon from '../../assets/icons/logo_bw.png';
import NavLanguageDropdown from "./NavLanguageDropdown";

export default class Nav extends Component {
    render() {
        const {pathname} = window.location;
        const { create , send, issue, info } = {
            create: pathname === '/' ? "active" : "",
            send: pathname.startsWith('/send_coins') ? "active" : "",
            issue: pathname.startsWith('/xdr_emission') ? "active" : "",
            info: pathname.startsWith('/wallet_info') ? "active" : "",
        };

        return (
            <nav className="navbar navbar-expand-md navbar-light">
                <div className="container">
                    <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar3SupportedContent">
                        <span className="navbar-toggler-icon"/>
                    </button>
                    <div className="collapse navbar-collapse text-center justify-content-center" id="navbar3SupportedContent">
                        <Link to={"/"}  className="navbar-brand text-secondary">
                            <img src={icon} alt="Logo" className="logo"/>
                        </Link>
                        <ul className="navbar-nav">
                            <Link to={"/"} className={`ml-3 btn navbar-btn btn-secondary ${create}`}>
                                <FormattedMessage id={local.createWallet} />
                            </Link><br/>
                            <Link to={"/send_coins"} className={`ml-3 btn navbar-btn btn-secondary ${send}`}>
                                <FormattedMessage id={local.sendCoins} />
                            </Link><br/>
                            {/* <Link to={"/xdr_emission"} className={`ml-3 btn navbar-btn btn-secondary ${issue}`}>
                                <FormattedMessage id={local.xdrEmission} />
                            </Link><br/> */}
                            <Link to={"/wallet_info"} className={`ml-3 btn navbar-btn btn-secondary ${info}`}>
                                <FormattedMessage id={local.walletInfo} />
                            </Link><br/>

                            <NavLanguageDropdown/>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}