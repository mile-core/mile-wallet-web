import React, {Component} from 'react';
import {Dropdown, DropdownItem, DropdownMenu, DropdownToggle} from "reactstrap";
import {Flag} from "semantic-ui-react";
import {FormattedMessage, injectIntl} from "react-intl";
import {connect} from "react-redux";
import local from "../../i18n/local";
import {setAppLanguage} from "../../action/app";

class NavLanguageDropdown extends Component {
    availableLanguages = [
        {
            key: 'en',
            icon: 'us',
            label: local.english
        },
        {
            key: 'kn',
            icon: 'kr',
            label: local.korean
        },
        {
            key: 'zh',
            icon: 'cn',
            label: local.chinese
        },
        {
            key: 'ja',
            icon: 'jp',
            label: local.japanese
        }
    ];

    constructor(props) {
        super(props);

        this.state = {
            isLanguageOpen: false
        }
    }

    toggleLanguage(v){
        this.setState({
            isLanguageOpen: !this.state.isLanguageOpen
        })
    }

    getAvailableLanguages(){
        return this.availableLanguages.filter(lang => lang.key !== this.props.language);
    }

    getCurrentLanguage(){
        return this.availableLanguages.find(lang => lang.key === this.props.language);
    }

    render() {

        const languages = this.getAvailableLanguages();
        const language = this.getCurrentLanguage();

        if(!language){
            return null;
        }

        return (
            <Dropdown className={'language-dropdown'}  nav isOpen={this.state.isLanguageOpen} toggle={this.toggleLanguage.bind(this)}>
                <DropdownToggle nav caret>
                    <Flag name={language.icon} /> <FormattedMessage id={language.label} />
                </DropdownToggle>
                <DropdownMenu>
                    <DropdownItem header>Choose language</DropdownItem>
                    {languages.map(lang => {
                        return (
                            <DropdownItem key={lang.key} onClick={() => {
                                this.props.setLanguage(lang.key);
                            }}>
                                <Flag name={lang.icon} /> <FormattedMessage id={lang.label} />
                            </DropdownItem>
                        )
                    })}
                </DropdownMenu>
            </Dropdown>
        );
    }
}

NavLanguageDropdown.propTypes = {};

export default connect(
    (state) => {
        return {
            language: state.app.language
        }
    },
    (dispatch) => {
        return {
            setLanguage: (language) => dispatch(setAppLanguage(language))
        }
    })(injectIntl(NavLanguageDropdown));
