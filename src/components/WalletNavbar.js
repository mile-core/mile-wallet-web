import React ,{ Component } from 'react';
import {FormattedMessage} from 'react-intl';
import {
    Nav,
    NavItem,
    NavLink
} from 'reactstrap';
import local from '../i18n/local';

export default class WalletNavbar extends Component {
    render() {
        return (
            <Nav tabs>
                <NavItem style={{margin:"0 auto"}}>
                    <NavLink href="/" active={this.props.match.path !== "/"}>
                        <FormattedMessage id={local.createWallet} />
                    </NavLink>
                </NavItem>
                <NavItem style={{margin:"0 auto"}}>
                    <NavLink href="/send_coins" active={this.props.match.path !== "/send_coins"}>
                        <FormattedMessage id={local.sendCoins} />
                    </NavLink>
                </NavItem>
                <NavItem style={{margin:"0 auto"}}>
                    <NavLink href="/xdr_emission" active={this.props.match.path !== "/xdr_emission"}>
                        <FormattedMessage id={local.xdrEmission} />
                    </NavLink>
                </NavItem>
                <NavItem style={{margin:"0 auto"}}>
                    <NavLink href="/wallet_info" active={this.props.match.path !== "/wallet_info"}>
                        <FormattedMessage id={local.walletInfo} />
                    </NavLink>
                </NavItem>
            </Nav>
        );
    }
}