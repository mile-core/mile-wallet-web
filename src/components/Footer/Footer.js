import React ,{ Component } from 'react';

export default class Footer extends Component {
    render() {
        return (
            <div className="py-4 w-footer">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 text-center">
                            <p className=""><a href="https://mile.global">MILE Unity</a></p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
