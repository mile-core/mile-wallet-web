import React from 'react';
import {injectIntl} from 'react-intl';


let instance = null;

// Does not actually render anything visible.
// We need it to provide access to internationalization for classes
// which are not a React component
@injectIntl
export default class CurrentLocale extends React.Component {
	componentWillMount() {
		instance = this;
	}

	render() {
		return false;
	}
}

export function intl() {
	return instance.props.intl;
}

export function formatMessage(...args) {
	return intl().formatMessage(...args);
}
