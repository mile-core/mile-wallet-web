import local from './local';
import i18n from './i18n';

const en = {};
const kn = {};
const ja = {};
const zh = {};

for (const key in local) {
    if (!local.hasOwnProperty(key)) {
        continue;
    }

    if(!i18n.hasOwnProperty(key)){
        console.warn(`No locale found ${key}`);
        continue;
    }

    const dict = i18n[key];

    en[key] = dict['en'];
    kn[key] = dict['kn'];
    ja[key] = dict['ja'];
    zh[key] = dict['zh'];
}

export default {kn, en, ja, zh};
