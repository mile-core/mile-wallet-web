import axios from 'axios';
import { APIUri } from './config';

class Api {
    constructor() {
        this.requestId = 0

        axios.get(APIUri.apiNodesList)
            .then( ans => {
                if( ans.status == 200 ){
                    this.fullList = ans.data
                }
                else {
                    throw 'Error'
                }
            })
            .catch( err => {
                this.fullList = [ APIUri.apiBaseUrl ]
            })
            .then( res => {
                this.fullList = this.fullList.map(e=>e+'/v1/api')
                this.baseUrl = []
                setTimeout( this.checkUrls(), 0 )
            })
    }

    getBaseUrl() {
        if( this.baseUrl.length == 0 ) {
            throw 'Check internet connection'
        }
        return this.baseUrl[ Math.trunc( Math.random() * this.baseUrl.length ) ];
    }

    checkUrls() {
        for( const url of this.fullList ) {
            this.requestNode( 'ping', {}, url )
            .then( () => {
                this.baseUrl.push( url )
            })
            .catch( () => {
                console.error( `Skipping node ${url}` )
            })
        }
    }


    requestNode( method, params = {}, node = null ) {
        this.requestId += 1

        const nodeUrl = node || this.getBaseUrl()

        return axios.post( nodeUrl, {
            method,
            params,
            id: this.requestId,
            jsonrpc: "2.0",
            version: "1.0"
        })
    }

    getBalance( publicKey ) {
        return this.requestNode(
            "get-wallet-state",
            { "public-key": publicKey }
        )
    }

    sendTransaction( transactionData ) {
        return this.requestNode(
            "send-transaction",
            transactionData
        )
    }

    getLastTransactions( publicKey, count = 1 ) {
        return this.requestNode(
            "get-wallet-transactions",
            { "public-key": publicKey, limit: count }
        )
    }

    getLastBlockId( ) {
        return this.requestNode(
            "get-current-block-id"
        )
    }
}

export default new Api();
