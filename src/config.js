const baseUrl = process.env.MILE_API_BASE || 'https://wallet.mile.global'

const paths = {
  apiBaseUrl: baseUrl,
  apiNodesList: baseUrl+'/v1/nodes.json',
}

const parameters = {
  memoLength: process.env.MILE_MEMO_LENGTH || 62,
}

export const APIUri = paths
export const sysParams = parameters